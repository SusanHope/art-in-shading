# Standard Basic Shading

If you want to learn how to draw or maybe sketch, there are simple steps on how to do it. You can sketch a simple subject starting with a shape. 

I have an example below that may help you, but before that, you have to see to it that the subject you wanted to draw or sketch is the thing you wanted most. Observe every line, point and corner of these examples and you will see the difference it makes. This art is made from pencil on paper, digital art, and [bespoke suits online](http://harrisandzei.com). 

![Screenshot of Asteroids](http://image.slidesharecdn.com/basicartlessonshapeandform-130826113208-phpapp01/95/basic-art-lesson-shape-and-form-5-638.jpg?cb=1377516775)

![Screenshot of Asteroids](http://2.bp.blogspot.com/_SP_kLrzk_Ts/TLH01XINRWI/AAAAAAAAAfs/z5RQUF0th60/s1600/poses.jpg)

![Screenshot of Asteroids](http://img09.deviantart.net/bedb/i/2009/032/2/c/draw_basic_people_part_2_by_ditroi.jpg)